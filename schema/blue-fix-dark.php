<?php
	
	if (! $nav_bg)
		$nav_bg = "#1C1C1C";
	if (! $nav_gradient_top)
		$nav_gradient_top = "#222";
	if (! $nav_gradient_bottom)
		$nav_gradient_bottom = "#222";
	if (! $nav_active_gradient_top)
		$nav_active_gradient_top = "#444";
	if (! $nav_active_gradient_bottom)
		$nav_active_gradient_bottom = "#333";
	if (! $nav_bd)
		$nav_bd = "#111";
	if (! $nav_icon_colour)
		$nav_icon_colour = "#999";
	if (! $nav_active_icon_colour)
		$nav_active_icon_colour = "#fff";
	if (! $link_colour)
		$link_colour = "grey";
	if (! $banner_colour)
		$banner_colour = "#999";
	if (! $bgcolour)
		$bgcolour = "#222";
	if (! $item_colour)
		$item_colour = "#333";
	if (! $comment_item_colour)
		$comment_item_colour = "#444";
	if (! $comment_border_colour)
		$comment_border_colour = "#333";
	if (! $toolicon_colour)
		$toolicon_colour = '#999';
	if (! $toolicon_activecolour)
		$toolicon_activecolour = '#fff';
	if (! $font_colour)
		$font_colour = "#ccc";
	if (! $converse_width)
		$converse_width = "1024";
		


