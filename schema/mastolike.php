<?php
	
	if (! $nav_bg)
		$nav_bg = "#13141A";
	if (! $nav_gradient_top)
		$nav_gradient_top = "#191B22";
	if (! $nav_gradient_bottom)
		$nav_gradient_bottom = "#191B22";
	if (! $nav_active_gradient_top)
		$nav_active_gradient_top = "#313543";
	if (! $nav_active_gradient_bottom)
		$nav_active_gradient_bottom = "#282C37";
	if (! $nav_bd)
		$nav_bd = "#111";
	if (! $nav_icon_colour)
		$nav_icon_colour = "#fff";
	if (! $nav_active_icon_colour)
		$nav_active_icon_colour = "#2B90D9";
	if (! $link_colour)
		$link_colour = "#2B90D9";
	if (! $banner_colour)
		$banner_colour = "#fff";
	if (! $bgcolour)
		$bgcolour = "#191B22";
	if (! $item_colour)
		$item_colour = "#282C37";
	if (! $comment_item_colour)
		$comment_item_colour = "#313543";
	if (! $comment_border_colour)
		$comment_border_colour = "#282C37";
	if (! $toolicon_colour)
		$toolicon_colour = '#fff';
	if (! $toolicon_activecolour)
		$toolicon_activecolour = '#2B90D9';
	if (! $font_colour)
		$font_colour = "#fff";
	if (! $converse_width)
		$converse_width = "1024";
		


