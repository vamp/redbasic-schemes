# Redbasic Schemes

Schemes for Fanzilla/Hubzilla theme 'Redbasic'

**Install:** download files and upload into view/theme/redbasic  
**Issues:** please report here or:  
**Contact:** [@vamp@hub.vamp.ink](https://hub.vamp.ink/channel/vamp "hubzilla") | [alt contact details](https://vamp.ink/contact.html)

## blue-fix

blue-fix is essentially a template scheme based 
on the redbasic default that removes the garish 
blue and replaces it with 'grey' for easy 
search and replace.

blue-fix also ups the default font-size and adds
styling to the 'location' post field that it can
be used for mood or whisperspace as used on DW
or tumblr.

![blue-fix screenshot](blue-fix.png)

## blue-fix-dark

blue-fix-dark is a template scheme based 
on the redbasic 'dark' scheme that removes the garish 
blue and replaces it with 'grey' for easy search and 
replace, also lightening the scheme somewhat as the 
original is pretty much black.

blue-fix-dark also ups the default font-size and adds
styling to the 'location' post field that it can
be used for mood or whisperspace as used on DW
or tumblr.

![blue-fix-dark screenshot](blue-fix-dark.png)

## mastolike

Blatantly imitating the default Mastodon colours. Based on blue-fix-dark.

![mastolike screenshot](mastolike.png)

## port-gore

Purple. Based on blue-fix.

![port-gore screenshot](port-gore.png)

## brutal

For science!

![brutal screenshot](brutal.png)

## 12-90

For [@oulfis](https://oulfis.space/channel/oulfis).

> sort of a mash-up between 'fussy grandmother' and 'rich 12-year-old girl'

![12-90 screenshot](12-90.png)

## sugarcane

Based on blue-fix.

![sugarcane screenshot](sugarcane.png)